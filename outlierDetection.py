#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This script finds all water level readings that are not between the previous and next value and differ at least 10cm from previous or next reading value and at least 2 times the difference between the previous and next value.

import json
import unicodecsv as csv
import io
import copy

delta = 0.1 # 10cm

with open("data.json") as f:
  data = json.load(f)

outlierCount = 0
for sensorName in data:
  print sensorName
  sensor = data[sensorName]
  for i in range(1,len(sensor["readings"])-1):
    prev = sensor["readings"][i-1]["water_level"]
    curr = sensor["readings"][i]["water_level"]
    next = sensor["readings"][i+1]["water_level"]

    difference = abs(prev-next)
    distFromPrev = abs(prev-curr)
    distFromNext = abs(next-curr)
    minDist = min(distFromPrev, distFromNext)

    # kas curr on prev ja next vahel?
    isBetweenU = prev <= curr <= next
    isBetweenD = prev >= curr >= next
    isBetween = isBetweenU or isBetweenD
    if not isBetween and minDist > delta and minDist > 2*difference:
      sensor["readings"][i]["water_level"] = (prev + next) / 2
      outlierCount+=1

print "Number of outliers: ", outlierCount

# write result to data.json and readings.csv
with io.open("data.json", "w", encoding="utf-8") as f:
    # data = json.dumps(sensors, f, separators=(',',': '), indent=2, sort_keys=True, ensure_ascii=False)
    jsonData = json.dumps(data, f, separators=(',',':'), sort_keys=True, ensure_ascii=False)
    f.write(unicode(jsonData))

print "JSON written"

with open("readings.csv", "wb") as f:
    writer = csv.DictWriter(f, fieldnames=["sensor", "timestamp", "water_level", "water_temperature"], encoding="utf-8")
    writer.writeheader()
    for sensor in data:
      for sr in data[sensor]["readings"]:
        sr["sensor"] = sensor
        writer.writerow(sr)

print "CSV written"