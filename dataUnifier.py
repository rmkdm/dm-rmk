#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import xlrd
import locale
import json
import unicodecsv as csv
import sys
from progress.bar import ShadyBar
from progress.spinner import PixelSpinner
import time
import threading
from datetime import datetime
import copy
import io

def parseFloat(str):
  try:
    return locale.atof(str.encode('ascii', 'ignore'))
  except ValueError:
    return float('nan')

def recordVanEssenSensor(sh):
  sensor = sh.cell_value(1,0)
  if sensor not in readings:
    if sensor in sensors:
      sensors[sensor]["serialnumber"] = sh.cell_value(1,1)
    else:
      sensors[sensor] = {"serialnumber": sh.cell_value(1,1)}
    readings[sensor] = []
  return sensor
def vanEssenDatetime(cell):
  if type(cell) is float:
    return xlrd.xldate_as_datetime(cell, 0)
  return datetime.strptime(cell, '%Y/%m/%d %H:%M:%S')
def vanEssenWaterLevel(cell):
  if cell=="":
    return float('nan')
  else:
    return cell/-100.0
def readVanEssenSheet(sh):
  sensor = recordVanEssenSensor(sh)
  for rx in range(1,sh.nrows):
    water_level = vanEssenWaterLevel(sh.cell_value(rx,4))
    if water_level!=water_level:
      continue # Leave out rows with NaN
    readings[sensor].append({
      "timestamp": vanEssenDatetime(sh.cell_value(rx,2)).isoformat(),
      "water_temperature": sh.cell_value(rx,3),
      "water_level": water_level})

def recordKellerSensors(sh):
  sensor1 = kellerSensor(sh.cell_value(1,1))
  if sensor1 not in readings:
    if sensor1 in sensors:
      sensors[sensor1]["serialnumber"] = kellerSN(sh.cell_value(0,1))
    else:
      sensors[sensor1] = {"serialnumber": kellerSN(sh.cell_value(0,1))}
    readings[sensor1] = []
  sensor2 = sh.cell_value(1,9) or sh.cell_value(1,2)
  serialnumber2 = sh.cell_value(0,9) or sh.cell_value(0,2)
  # if sensor2 not in readings:
  #   if sensor2 in sensors:
  #     sensors[sensor2]["serialnumber"] = serialnumber2
  #   else:
  #     sensors[sensor2] = {"serialnumber": serialnumber2}
  #   readings[sensor2] = []
  return [sensor1, sensor2]
def kellerDatetime(date_cell, time_cell):
  date = datetime.strptime(date_cell, "%d.%m.%Y")
  time = datetime.strptime(time_cell, "%H:%M:%S")
  return datetime.combine(date, time.time())
def kellerSN(cell):
  if cell.find("Serial Number") >= 0:
    return cell[:cell.find("Serial Number")]
  else:
    return cell
def kellerSensor(cell):
  if cell.find("Device Identity") >= 0:
    return cell[:cell.find("Device Identity")]
  else:
    return cell
def kellerWaterLevel(cell):
  return parseFloat(cell)*-1.0
def readKellerSheet(sh):
  [sensor1, sensor2] = recordKellerSensors(sh)
  for rx in range(9,sh.nrows):
    if sh.cell_value(rx,2):
      water_level = kellerWaterLevel(sh.cell_value(rx,4))
      if water_level!=water_level:
        continue # Leave out rows with NaN
      readings[sensor1].append({
        "timestamp": kellerDatetime(sh.cell_value(rx,2), sh.cell_value(rx,3)).isoformat(),
        "water_temperature": parseFloat(sh.cell_value(rx,6)),
        "water_level": water_level
        # "water_pressure": parseFloat(sh.cell_value(rx,5))
      })
    # if sh.cell_value(rx,10) != "": # Mõnes failis on teise sensori ridu vähem ja siis viskab datetime'i parser errorit
    #   readings[sensor2].append({
    #     "timestamp": kellerDatetime(sh.cell_value(rx,10), sh.cell_value(rx,11)).isoformat(),
    #     "air_temperature": parseFloat(sh.cell_value(rx,13)),
    #     "air_pressure": parseFloat(sh.cell_value(rx,12))})

def readSensorLocations(sh):
  for rx in range(1,sh.nrows):
    name = sh.cell_value(rx,2)
    x = sh.cell_value(rx,0)
    y = sh.cell_value(rx,1)
    if name not in sensors:
      sensors[name] = {
        "lat": y,
        "lng": x}
    else:
      sensors[name]["lat"] = x
      sensors[name]["lng"] = y

def readSheet(sh):
  if sh.ncols == 5:
    readVanEssenSheet(sh)
  elif sh.ncols == 3:
    readSensorLocations(sh)
  else:
    readKellerSheet(sh)
def readWorkbook(wb):
  readSheet(wb.sheet_by_index(0))
def gatherFromFile(path):
  readWorkbook(xlrd.open_workbook(path, logfile=open(os.devnull, 'w')))
def xlsFilesInDir(path):
  return [fName for fName in os.listdir(path) if fName.split(".")[-1].lower() in ["xls", "xlsx"]]
def visit(bar, dirname, names):
  for f in xlsFilesInDir(dirname):
    bar.next()
    gatherFromFile(dirname+"/"+f)
def countFiles(arg, dirname, names):
  global fileCount
  fileCount += len(xlsFilesInDir(dirname))
def countXLSFiles():
  global fileCount
  fileCount = 0
  os.path.walk(".", countFiles, 0)
  return fileCount
def writeJSON(sensors, readings):
  for sensor in sensors:
    sensors[sensor]["readings"] = readings[sensor]
    del readings[sensor]
  with io.open("data.json", "w", encoding="utf-8") as f:
    # data = json.dumps(sensors, f, separators=(',',': '), indent=2, sort_keys=True, ensure_ascii=False)
    data = json.dumps(sensors, f, separators=(',',':'), sort_keys=True, ensure_ascii=False)
    f.write(unicode(data))
def writeCSV(sensors, readings):
  with open("sensors.csv", "wb") as f:
    writer = csv.DictWriter(f, fieldnames=["name", "serialnumber", "lat", "lng"], encoding="utf-8")
    writer.writeheader()
    for sensor in sensors:
      row = copy.copy(sensors[sensor])
      row["name"] = sensor
      writer.writerow(row)
  with open("readings.csv", "wb") as f:
    writer = csv.DictWriter(f, fieldnames=["sensor", "timestamp", "water_level", "water_temperature"], encoding="utf-8")
    writer.writeheader()
    for sensor in readings:
      sensorReadings = copy.deepcopy(readings[sensor])
      for sr in sensorReadings:
        sr["sensor"] = sensor
        writer.writerow(sr)
def writeResult(sensors, readings):
  writeCSV(sensors, readings)
  writeJSON(sensors, readings)

locale.setlocale(locale.LC_ALL, 'et_EE.UTF-8')
sensors = {}
readings = {}

bar = ShadyBar('Kogun andmeid failidest', max=countXLSFiles())
os.path.walk(".", visit, bar)
bar.finish()

sensorsWithoutLocation = []
sensorsWithoutSN = []
for sensor in sensors:
  if "serialnumber" not in sensors[sensor]:
    sensorsWithoutSN.append(sensor)
  if "lat" not in sensors[sensor]:
    sensorsWithoutLocation.append(sensor)
for sensor in sensorsWithoutLocation:
  del sensors[sensor]
  del readings[sensor]
for sensor in sensorsWithoutSN:
  del sensors[sensor]
for sensor in sensors:
  readings[sensor] = sorted(readings[sensor], key=lambda k: k["timestamp"])

spinner = PixelSpinner('Kirjutan kogutud andmed failidesse data.json, sensors.csv ja readings.csv ')
t = threading.Thread(target=writeResult, args=(sensors,readings))
t.start()
while t.isAlive():
  time.sleep(0.25)
  spinner.next()
spinner.finish()

print "\nValmis!"

print "Sensors without location: "
print sorted(sensorsWithoutLocation)
print "Sensors without readings: "
print sorted(sensorsWithoutSN)