# README #

RMK mire drainage elimination analysis. We received data from RMK about water levels in mires. The raw data came in xlsx files and was from two different sensors. A python script was developed to merge these two data formats.

### Merging the two data formats ###

``` bash
> python dataUnifier.py
```